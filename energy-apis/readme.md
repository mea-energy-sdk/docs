![](./static/logo-240.png)

**_Energy for city life, Energize smart living_**

---

<br>

# Energy APIs

- [ข้อมูลค่าไฟฟ้าเดือนปัจจุบัน](#ข้อมูลค่าไฟฟ้าเดือนปัจจุบัน)
- [ข้อมูลประวัติการใช้งานไฟฟ้าย้อนหลัง 6 เดือน](#ข้อมูลประวัติการใช้งานไฟฟ้าย้อนหลัง-6-เดือน)
- [ข้อมูลรายการแจ้งเตือน](#ข้อมูลรายการแจ้งเตือน)
- [Webhook ข้อมูลรายการแจ้งเตือน](./webhook/readme.md)
- [ลงทะเบียนเครื่องวัดฯ](#ลงทะเบียนเครื่องวัดฯ)

<br>

# ข้อมูลค่าไฟฟ้าเดือนปัจจุบัน

```
GET {{baseUrl}}/billing/enquiry
```

### Request Headers

- `X-Api-Key` API Key ที่ได้รับจากการไฟฟ้านครหลวง
- `X-Client-Code` Client Code ที่ได้รับจากการไฟฟ้านครหลวง

### Query Parameters

- `ca` เลขที่แสดงสัญญา (CA)
- `requireQRCodeString` ต้องการคืนค่าข้อมูลสำหรับสร้าง QR Code ชำระค่าไฟฟ้า
- `requireQRCodeHint` ต้องการคืนค่าคำอธิบายสำหรับ QR Code ชำระค่าไฟฟ้า

### Response Body

- `meta` Metadata
- `data` ข้อมูลผลลัพธ์ ประกอบด้วย

| Name          | Type   | Description                                                              |
| ------------- | ------ | ------------------------------------------------------------------------ |
| amount        | Number | จำนวนเงินค่าไฟฟ้า                                                        |
| disconnectAmt | Number | ค่าดำเนินการงดจ่ายไฟ                                                     |
| dueDate       | String | วันที่ครบกำหนดชำระ ในรูปแบบ YYYY-MM-DD                                   |
| lastUnit      | Number | จำนวนหน่วยไฟฟ้าของรอบบิลก่อนหน้า                                         |
| message       | String | คำอธิบายเพิ่มเติม                                                        |
| qrCodeHint    | String | คำอธิบายสำหรับ QR Code ชำระค่าไฟฟ้า (กรณี requireQRCodeHint = true)      |
| qrCodeString  | String | ข้อมูลสำหรับสร้าง QR Code ชำระค่าไฟฟ้า (กรณี requireQRCodeString = true) |
| totalInt      | Number | ค่าเบี้ยปรับผิดนัด                                                       |

### Response Example

```json
{
  "meta": {
    "copyright": "Copyright 2022 The Metropolitan Electricity Authority. All rights reserved."
  },
  "data": {
    "amount": 1088.1,
    "disconnectAmt": 0,
    "dueDate": "2022-04-16",
    "lastUnit": 260,
    "message": null,
    "qrCodeHint": "ชำระที่การไฟฟ้านครหลวง, เทสโก้โลตัส, แฟมิลี่มาร์ท, ท็อปส์ และบิ๊กซี",
    "qrCodeString": "|099400016520011<CR>123456789000002600<CR>022441960422160465<CR>108810",
    "totalInt": 0
  }
}
```

<br/>
<br/>

# ข้อมูลประวัติการใช้งานไฟฟ้าย้อนหลัง 6 เดือน

```
GET {{baseUrl}}/consumption-history/history
```

### Request Headers

- `X-Api-Key` API Key ที่ได้รับจากการไฟฟ้านครหลวง
- `X-Client-Code` Client Code ที่ได้รับจากการไฟฟ้านครหลวง

### Query Parameters

- `ca` เลขที่แสดงสัญญา (CA)
- `type` ประเภทข้อมูลที่คืนค่า เช่น จำนวนเงินค่าไฟฟ้า, จำนวนหน่วยไฟฟ้า โดยค่าที่ระบุได้ ประกอบด้วย
  - `amount` จำนวนเงินค่าไฟฟ้า
  - `unit` จำนวนหน่วยไฟฟ้า
  - `all` จำนวนเงินค่าไฟฟ้า และจำนวนหน่วยไฟฟ้า

### Response Body

- `meta` Metadata
- `data` รายการผลลัพธ์ ประกอบด้วย

| Name   | Type   | Description                                                   |
| ------ | ------ | ------------------------------------------------------------- |
| year   | Number | ปี ค.ศ. ของข้อมูล                                             |
| month  | Number | เดือนของข้อมูล โดยเดือนมกราคมเป็นลำดับที่ 0                   |
| amount | Number | จำนวนเงินค่าไฟฟ้า (กรณีกำหนด `type` เป็น `amount` หรือ `all`) |
| unit   | Number | จำนวนหน่วยไฟฟ้า (กรณีกำหนด `type` เป็น `unit` หรือ `all`)     |

### Response Example

```json
{
  "meta": {
    "copyright": "Copyright 2022 The Metropolitan Electricity Authority. All rights reserved."
  },
  "data": [
    {
      "year": 2021,
      "month": 9,
      "amount": 964.48,
      "unit": 228
    },
    {
      "year": 2021,
      "month": 10,
      "amount": 1314.94,
      "unit": 302
    },
    {
      "year": 2021,
      "month": 11,
      "amount": 855.55,
      "unit": 205
    },
    {
      "year": 2022,
      "month": 0,
      "amount": 528.74,
      "unit": 136
    },
    {
      "year": 2022,
      "month": 1,
      "amount": 805.75,
      "unit": 199
    },
    {
      "year": 2022,
      "month": 2,
      "amount": 1088.1,
      "unit": 260
    }
  ]
}
```

<br/>
<br/>

# ข้อมูลรายการแจ้งเตือน

```
GET {{baseUrl}}/messaging/message
```

### Request Headers

- `X-Api-Key` API Key ที่ได้รับจากการไฟฟ้านครหลวง
- `X-Client-Code` Client Code ที่ได้รับจากการไฟฟ้านครหลวง

### Query Parameters

- `ca` เลขที่แสดงสัญญา (CA)
- `limit` จำนวนรายการแจ้งเตือนที่ต้องการ หากไม่กำหนดจะคืนค่า 3 รายการแจ้งเตือนล่าสุด
- `categories` ประเภทแจ้งเตือนที่ต้องการ คั่นด้วย , เช่น `BILLING,WARNING` โดยประเภทของข้อมูล ประกอบด้วย
  - `BILLING` แจ้งเตือนค่าไฟฟ้าตามรอบบิล
  - `WARNING` แจ้งเตือนให้ชำระค่าไฟฟ้า
  - `OUTAGE` แจ้งเตือนประกาศดับไฟฟ้า
  - `NOTIOUTAGE` แจ้งเตือนเมื่อแจ้งไฟฟ้าดับ (ไฟฟ้าขัดข้อง) ไปยังการไฟฟ้านครหลวงสำเร็จ
  - `SENTOUTAGE` แจ้งเตือนเมื่อไฟฟ้านครหลวงแก้ไขไฟฟ้าขัดข้องเสร็จ
  - `BILLED_ACP` แจ้งเตือนการชำระค่าไฟฟ้าสำเร็จ
  - `BILLED_REJ` แจ้งเตือนการชำระค่าไฟฟ้าไม่สำเร็จ

### Response Body

- `meta` Metadata
- `data` รายการผลลัพธ์ ประกอบด้วย

| Name        | Type   | Description                         |
| ----------- | ------ | ----------------------------------- |
| ca          | String | เลขที่แสดงสัญญา (CA)                |
| detail      | String | รายละเอียดข้อความแจ้งเตือน          |
| messageDate | String | วันที่แจ้งเตือน ในรูปแบบ ISO String |
| messageId   | String | รหัสข้อความแจ้งเตือน                |
| messageType | String | ประเภทข้อความแจ้งเตือน              |
| meterNo     | String | รหัสเครื่องวัดฯ                     |
| title       | String | หัวข้อข้อความแจ้งเตือน              |

### Response Example

```json
{
  "meta": {
    "copyright": "Copyright 2022 The Metropolitan Electricity Authority. All rights reserved."
  },
  "data": [
    {
      "ca": "123456789",
      "detail": "CA123456789 ค่าไฟฟ้า 1,088.10 บาท ส่งหักบัตรเครดิตเลขที่ xx-1234 วันที่ 23/03/2565 / CA123456789 The electricity bill balance is 1,088.10 THB It will be collected by Direct Debit from your credit card no. xx-1234 on 23/03/2022",
      "messageDate": "2022-03-23T05:01:10.597Z",
      "messageId": "E5328D10-F081-44B6-B735-5C46D3A15DF4",
      "messageType": "BILLING",
      "meterNo": "12345678",
      "title": "แจ้งค่าไฟฟ้า"
    },
    {
      "ca": "123456789",
      "detail": "CA123456789 ค่าไฟฟ้า 805.75 บาท ส่งหักบัตรเครดิตเลขที่ xx-1234 วันที่ 23/02/2565 / CA123456789 The electricity bill balance is 805.75 THB It will be collected by Direct Debit from your credit card no. xx-1234 on 23/02/2022",
      "messageDate": "2022-02-23T05:01:10.597Z",
      "messageId": "F5328D10-F081-44B6-B735-5C46D3A15DF4",
      "messageType": "BILLING",
      "meterNo": "12345678",
      "title": "แจ้งค่าไฟฟ้า"
    }
  ]
}
```

<br/>
<br/>

# ลงทะเบียนเครื่องวัดฯ

```
PUT {{baseUrl}}/register/register-ca
```

### Request Headers

- `X-Api-Key` API Key ที่ได้รับจากการไฟฟ้านครหลวง
- `X-Client-Code` Client Code ที่ได้รับจากการไฟฟ้านครหลวง

### Request Body

- `ca` เลขที่แสดงสัญญา (CA)
- `projectCode` รหัสโครงการที่อยู่อาศัย ของเลขที่แสดงสัญญาที่กำหนด (ต้องเป็นข้อมูลเดียวกับที่ให้ไว้กับการไฟฟ้านครหลวง)
- `projectName` ชื่อโครงการที่อยู่อาศัย ของเลขที่แสดงสัญญาที่กำหนด
- `projectAddress` ที่อยู่โครงการที่อยู่อาศัย ของเลขที่แสดงสัญญาที่กำหนด

### Response Body

- `meta` Metadata
- `data` ข้อมูลผลลัพธ์ ประกอบด้วย

| Name      | Type   | Description                                |
| --------- | ------ | ------------------------------------------ |
| projectId | String | รหัสประจำข้อมูลโครงการ                     |
| meterNo   | String | รหัสเครื่องวัดฯ ของเลขที่แสดงสัญญาที่กำหนด |

### Response Example

```json
{
  "meta": {
    "copyright": "Copyright 2022 The Metropolitan Electricity Authority. All rights reserved."
  },
  "data": {
    "projectId": "me2b3471-edbd-4b02-ad66-765c4e28876d",
    "meterNo": "12345678"
  }
}
```

<br/>
<br/>
<br/>

![](./static/device-1.png)
