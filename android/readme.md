![](./static/logo-240.png)

**_Energy for city life, Energize smart living_**

---

<br/>

# Android Energy SDK

- [วิธีการติดตั้ง](#วิธีการติดตั้ง)
- [วิธีการเรียกใช้งาน](#วิธีการเรียกใช้งาน)
  - [Initialization](#initialization)
  - [แสดงข้อมูลจากการไฟฟ้านครหลวง](#แสดงข้อมูลจากการไฟฟ้านครหลวง)
    - [แสดงข้อมูลทั้งหมดของเครื่องวัดฯ](#แสดงข้อมูลทั้งหมดของเครื่องวัดฯ)
    - [แสดงค่าไฟฟ้าเดือนปัจจุบัน](#แสดงค่าไฟฟ้าเดือนปัจจุบัน)
    - [แสดงประวัติการใช้งานไฟฟ้าย้อนหลัง 6 เดือน](#แสดงประวัติการใช้งานไฟฟ้าย้อนหลัง-6-เดือน)
    - [แสดงรายการแจ้งเตือน](#แสดงรายการแจ้งเตือน)
- [วิธีการลงทะเบียนเครื่องวัดฯ](#วิธีการลงทะเบียนเครื่องวัดฯ)
- [วิธีการเลือก Theme](#วิธีการเลือก-theme])
- [วิธีหาเลขที่แสดงสัญญา (CA) จาก QR Code ในบิลค่าไฟฟ้า](#วิธีหาเลขที่แสดงสัญญา-ca-จาก-qr-code-ในบิลค่าไฟฟ้า)
- [API Reference](./reference.md)

<br>

# วิธีการติดตั้ง

1. เปิด Project ที่ต้องการใช้งาน SDK
2. สร้าง Token ที่ได้รับจากการไฟฟ้านครหลวง ในไฟล์ gradle.properties ดังนี้
   - สำหรับ macOS/Linux เพิ่มที่ไฟล์ `$HOME/.gradle/gradle.properties`
   - สำหรับ Windows เพิ่มที่ไฟล์ `C:\Users\<Your User>\.gradle\gradle.properties`
```
authToken=<MEA Token>
```
3. เปิดไฟล์ build.gradl ที่ระดับ Project และเพิ่ม Repository ตามข้อมูลด้านล่าง

```
allprojects {
    repositories {
        ....
        maven {
          url 'https://jitpack.io'
          credentials { username authToken }
        }
    }
}
```

4. เปิดไฟล์ build.gradl ที่ระดับ Module และเพิ่ม Dependency ตามข้อมูลด้านล่าง

```
dependencies {
    ...
    implementation 'com.gitlab.mea-energy-sdk:android-energy-sdk:1.1.3'
}
```

5. Sync project โคยคลิกปุ่ม **Sync Now** เพื่อดาวน์โหลด package

6. เปิดไฟล์ AndroidManifest.xml และเพิ่ม Source Code ดามด้านล่าง

```xml
<manifest ...>
    <uses-permission android:name="android.permission.INTERNET" />
    <application
        ...
        android:usesCleartextTraffic="true"
        ...>
        ...
    </application>
</manifest>
```

<br/>

# วิธีการเรียกใช้งาน

## Initialization

Initialize SDK พร้อมกับกำหนด Environment, API Key และ Client Code ที่ได้รับจากการไฟฟ้านครหลวง ควรกำหนดตั้งแต่ที่ Application เริ่มทำงาน

```java
MEAEnergy.getInstance().setInitializeApp(this) // or MEAEnergy.getInstance().setInitializeApp(applicationContext)
MEAEnergy.getInstance().setAPIKey("Your API Key", "CLIENT CODE")
MEAEnergy.getInstance().setEnv(Env.PRODUCTION)
```

สามารถกำหนด Environment ได้ดังนี้

```java
MEAEnergy.getInstance().setEnv(Env.STAGING) // Staging (default)
MEAEnergy.getInstance().setEnv(Env.PRODUCTION) // Production
```

<br/>

## แสดงข้อมูลจากการไฟฟ้านครหลวง

> ทุกเครื่องวัดฯ ที่ต้องการใช้งาน จะต้อง [ลงทะเบียน](#วิธีการลงทะเบียนเครื่องวัดฯ) กับการไฟฟ้านครหลวงในครั้งแรกก่อนเริ่มต้นใช้งาน

<br/>

### แสดงข้อมูลทั้งหมดของเครื่องวัดฯ

1. เพิ่มการแสดงผล Widget โดยเปิดไฟล์ XML Layout ที่ต้องการ และเพิ่ม Soruce Code ดังนี้

```xml
<com.mea.energysdk.widget.MEAEnergyWidget
    android:id="@+id/meaWidget"
    android:layout_width="match_parent"
    android:layout_height="wrap_content" />
```

2. กำหนดบ้านหรือคอนโดที่ต้องการแสดงข้อมูล โดยกำหนดเลขที่แสดงสัญญา (CA) ที่ `"Your CA"`

```java
meaWidget.initCA("Your CA", object : MEAEnergyWidgetCallback {
    override fun onDone() {
        //load data done
    }

    override fun onFail(errors: JSONArray?) {
        // errors example [{"code":"UNAUTHORIZED","title":"Unauthorized"}]
    }
})
```

<br/>

### แสดงค่าไฟฟ้าเดือนปัจจุบัน

1. เพิ่มการแสดงผล Widget โดยเปิดไฟล์ XML Layout ที่ต้องการ และเพิ่ม Soruce Code ดังนี้

```xml
<com.mea.energysdk.widget.MEAEnergyWidgetBilling
    android:id="@+id/billing"
    android:layout_width="match_parent"
    android:layout_height="wrap_content" />
```

2. กำหนดบ้านหรือคอนโดที่ต้องการแสดงข้อมูล โดยกำหนดเลขที่แสดงสัญญา (CA) ที่ `"Your CA"`

```java
billing.initCA("Your CA",object : MEAEnergyWidgetCallback{
    override fun onDone() {
        //load data done
    }
    override fun onFail(errors: JSONArray?) {
         //errors example [{"code":"UNAUTHORIZED","title":"Unauthorized"}]
    }
})
```

<br/>

### แสดงประวัติการใช้งานไฟฟ้าย้อนหลัง 6 เดือน

1. เพิ่มการแสดงผล Widget โดยเปิดไฟล์ XML Layout ที่ต้องการ และเพิ่ม Soruce Code ดังนี้

```xml
<com.mea.energysdk.widget.MEAEnergyWidgetHistory
    android:id="@+id/history"
    android:layout_width="match_parent"
    android:layout_height="wrap_content" />
```

2. กำหนดบ้านหรือคอนโดที่ต้องการแสดงข้อมูล โดยกำหนดเลขที่แสดงสัญญา (CA) ที่ `"Your CA"`

```java
history.initCA("Your CA",object : MEAEnergyWidgetCallback{
    override fun onDone() {
        //load data done
    }
    override fun onFail(errors: JSONArray?) {
         //errors example [{"code":"UNAUTHORIZED","title":"Unauthorized"}]
    }
})
```

<br/>

### แสดงรายการแจ้งเตือน

1. เพิ่มการแสดงผล Widget โดยเปิดไฟล์ XML Layout ที่ต้องการ และเพิ่ม Soruce Code ดังนี้

```xml
<com.mea.energysdk.widget.MEAEnergyWidgetMessaging
    android:id="@+id/messaging"
    android:layout_width="match_parent"
    android:layout_height="wrap_content" />
```

2. กำหนดบ้านหรือคอนโดที่ต้องการแสดงข้อมูล โดยกำหนดเลขที่แสดงสัญญา (CA) ที่ `"Your CA"`

```java
messaging.initCA("Your CA",object : MEAEnergyWidgetCallback{
    override fun onDone() {
        //load data done
    }
    override fun onFail(errors: JSONArray?) {
         //errors example [{"code":"UNAUTHORIZED","title":"Unauthorized"}]
    }
})
```

<br/>

> ทุก ๆ Compoent สามารถเลือก theme ให้เหมาะสมแก่การใช้งานได้มากยิ่งขึ้น ศึกษาวิธีการเปลี่ยน theme ได้จาก [วิธีการเลือก Theme](#วิธีการเลือก-theme])

<br/>

# วิธีการลงทะเบียนเครื่องวัดฯ

> การลงทะเบียนเครื่องวัดฯ จะทำ **เฉพาะการเข้าใช้งานครั้งแรกของเครื่องวัดฯ** เท่านั้น

```java
MEAEnergy.getInstance().registerCA(
    "Your CA", // เลขที่แสดงสัญญา (CA)
    "Project Code", // รหัสโครงการ
    "Project Name", // ชื่อโครงการภาษาไทย
    "Project Name EN", // ชื่อโครงการภาษาอังกฤษ
    "Project Address", // ที่อยู่โครงการภาษาไทย
    "Project Address EN", // ที่อยู่โครงการภาษาอังกฤษ
    object : MEAEnergyCallback {
          override fun onSuccess(data: JSONObject?) {
                // data.projectId
                // data.meterNo
          }

          override fun onFail(errors: JSONArray?) {
              // errors example [{"code":"UNAUTHORIZED","title":"Unauthorized"}]
          }
})
```

<br/>

# วิธีการเลือก Theme

MEA Energy SDK ประกอบด้วย 3 รูปแบบการแสดงผล คือ MEA, Light และ Dark เพื่อรองรับกับความหลากหลายในการใช้งาน ผู้ใช้งานสามารถเลือกใช้ Theme ให้สอดคล้องกับ Application ของตนได้ หากไม่กำหนด Theme **MEA** จะถูกกำหนดโดยอัตโนมัติ

การกำหนด Theme สามารถทำได้โดยการเรียก Method `setTheme(theme)` และระบุ Theme ที่ต้องการลงบน Parameter `theme` ตัวอย่าง เช่น

```java
meaWidget.setTheme(MEAEnergyTheme.DARK)
```

โดย Theme ที่กำหนดได้ทั้งหมดมีดังนี้

```java
MEAEnergyTheme.MEA // MEA theme (default)
MEAEnergyTheme.LIGHT // Light theme
MEAEnergyTheme.DARK // Dark theme
```

![](./static/all-theme.jpg)

<br/>

# วิธีหาเลขที่แสดงสัญญา (CA) จาก QR Code ในบิลค่าไฟฟ้า

MEA Energy SDK อำนวยความสะดวกให้ผู้ใช้งานในการค้นหาและเรียกใช้เลขที่แสดงสัญญา (CA) จาก QR Code ในบิลค่าไฟฟ้าของการไฟฟ้านครหลวง โดยผู้ใช้งานส่ง String ของ QR Code ในบิลค่าไฟฟ้าเข้า Method `getCAFromQRCode()` ตัวอย่างเช่น

```java
val ca = MEAEnergyUtils.getCAFromQRCode("Your QR Code String")
```

<br/>
<br/>
<br/>
<br/>

![](./static/device-1.png)
