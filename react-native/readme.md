![](./static/logo-240.png)

**_Energy for city life, Energize smart living_**

---

<br/>

# React Native Energy SDK

- [วิธีการติดตั้ง](#วิธีการติดตั้ง)
- [ตัวอย่างการเรียกใช้งาน](#ตัวอย่างการเรียกใช้งาน)
- [แสดงข้อมูลจากการไฟฟ้านครหลวง](#แสดงข้อมูลจากการไฟฟ้านครหลวง)
  - [แสดงข้อมูลทั้งหมดของเครื่องวัดฯ](#แสดงข้อมูลทั้งหมดของเครื่องวัดฯ)
  - [แสดงค่าไฟฟ้าเดือนปัจจุบัน](#แสดงค่าไฟฟ้าเดือนปัจจุบัน)
  - [แสดงประวัติการใช้งานไฟฟ้าย้อนหลัง 6 เดือน](#แสดงประวัติการใช้งานไฟฟ้าย้อนหลัง-6-เดือน)
  - [แสดงรายการแจ้งเตือน](#แสดงรายการแจ้งเตือน)
- [SDK Theme](#sdk-theme)

<br/>

# วิธีการติดตั้ง

#### 1. Download MEA Energy SDK Component (https://gitlab.com/mea-energy-sdk/react-native-energy-sdk/-/archive/v1.1.0/react-native-energy-sdk-v1.1.0.zip)

#### 2. Unzip และ copy compoent ไปยัง path ที่ต้องการ เช่น `src/components/MEAEnergy`

#### 3. Linking assets folder

**กรณี React Native < 0.60**

- เปิดไฟล์ package.json

- เพิ่ม Reference ตามรายละเอียด ดังนี้

```json
"rnpm": {
   "assets": ["./<Path to your MEAEnergy>/assets/fonts/"]
}
```

- Run คำสั่งด้านล่างใน Terminal

```
$ react-native link
```

**กรณี React Native >= 0.60**

- เปิดไฟล์ react-native.config.js (หรือสร้างไฟล์ใหม่) และเพิ่ม Reference ตามรายละเอียด ดังนี้

```js
module.exports = {
   ...
   assets: ['./<Path to your MEAEnergy>/assets/fonts/']
   ...
};
```

- Run คำสั่งด้านล่างใน Terminal

```
$ react-native link
```

#### 4. ติดตั้ง modules เพิ่มเติม

4.1 Moment.js

```
$ npm i moment
```

4.2 react-native-svg (**[More information](https://github.com/react-native-community/react-native-svg#installation)**)

- Install module

```
$ npm i react-native-svg
```

- Link native code

**กรณี React Native < 0.60** พิมพ์คำสั่งด้านล่างใน Terminal

```
$ react-native link react-native-svg
```

**กรณี React Native >= 0.60** พิมพ์คำสั่งด้านล่างใน Terminal

```
$ cd ios && pod install
```

4.3 react-native-qrcode-svg

```
$ npm i react-native-qrcode-svg
```

4.4 react-native-barcode-builder (**[More Information](https://www.npmjs.com/package/react-native-barcode-builder)**)

4.5 react-native-device-info (**[More Information](https://www.npmjs.com/package/react-native-device-info#installation)**)

4.6 victory-native (**[More Information](https://www.npmjs.com/package/victory-native)**)

4.7 @react-native-community/art (**[More Information](https://www.npmjs.com/package/@react-native-community/art)**)

4.8 @react-native-community/async-storage (**[More Information](https://www.npmjs.com/package/@react-native-community/async-storage)**) // v1.1.0 or later

4.9 numeral (**[More Information](https://www.npmjs.com/package/numeral)**) // v1.1.0 or later

4.10 react-native-modal (**[More Information](https://www.npmjs.com/package/react-native-modal)**) // v1.1.0 or later

**โดย version ของ modules ที่แนะนำเป็นดังนี้**

```json
{
  "@react-native-community/art": "^1.2.0",
  "@react-native-community/async-storage": "^1.12.0",
  "moment": "^2.29.1",
  "numeral": "^2.0.6",
  "react": "16.13.1",
  "react-native": "0.63.4",
  "react-native-barcode-builder": "^2.0.0",
  "react-native-device-info": "^8.0.0",
  "react-native-modal": "^11.6.1",
  "react-native-qrcode-svg": "^6.0.6",
  "react-native-svg": "^12.1.0",
  "victory-native": "^35.3.1"
}
```

<br/><br/>

# ตัวอย่างการเรียกใช้งาน

กำหนดข้อมูลสำหรับการ initial

```js
import MEAEnergy, { ENV, setEnv, setApiKey, setClientCode } from './<Path to your MEAEnergy>'

// Initial
setEnv('<Your Environment>')
setApiKey('<Your API Key>')
setClientCode('<Your Client Code>')

// Use component
<MEAEnergy
  ca="<Your CA>"
  theme="<Your Theme>"
  onError={errors => {
    console.log('onError: ', errors)
  }}
/>
```

สามารถกำหนด Environment ได้ดังนี้

```js
ENV.STAGING; // Staging environment (default)
ENV.PRODUCTION; // Production environment

// Example
setEnv(ENV.PRODUCTION);
```

<br/><br/>

# แสดงข้อมูลจากการไฟฟ้านครหลวง

## แสดงข้อมูลทั้งหมดของเครื่องวัดฯ

```js
import MEAEnergy, { ENV, setEnv, setApiKey, setClientCode } from './<Path to your MEAEnergy>';

<MEAEnergy
  ca='<Your CA>'
  theme='<Your Theme>'
  onError={(errors) => {
    console.log('onError: ', errors);
  }}
/>;
```

### Props

- `ca` บัญชีแสดงสัญญา (CA) ของบ้าน/คอนโด ที่ต้องการแสดงข้อมูล
- `theme` Theme ที่เลือกใช้งาน ([รายการ Theme ทั้งหมด](#sdk-theme))
- `onError` Callback error function

<br/><br/>

## แสดงค่าไฟฟ้าเดือนปัจจุบัน

```js
import { MEAEnergyBilling } from './<Path to your MEAEnergy>';

<MEAEnergyBilling
  ca='<Your CA>'
  theme='<Your Theme>'
  onError={(errors) => {
    console.log('onError: ', errors);
  }}
/>;
```

### Props

- `ca` บัญชีแสดงสัญญา (CA) ของบ้าน/คอนโด ที่ต้องการแสดงข้อมูล
- `theme` Theme ที่เลือกใช้งาน ([รายการ Theme ทั้งหมด](#sdk-theme))
- `onError` Callback error function

<br/><br/>

## แสดงประวัติการใช้งานไฟฟ้าย้อนหลัง 6 เดือน

```js
import { MEAEnergyHistory } from './<Path to your MEAEnergy>';

<MEAEnergyHistory
  ca='<Your CA>'
  theme='<Your Theme>'
  onError={(errors) => {
    console.log('onError: ', errors);
  }}
/>;
```

### Props

- `ca` บัญชีแสดงสัญญา (CA) ของบ้าน/คอนโด ที่ต้องการแสดงข้อมูล
- `theme` Theme ที่เลือกใช้งาน ([รายการ Theme ทั้งหมด](#sdk-theme))
- `onError` Callback error function

<br/><br/>

## แสดงรายการแจ้งเตือน

```js
import { MEAEnergyMessaging } from './<Path to your MEAEnergy>';

<MEAEnergyMessaging
  ca='<Your CA>'
  theme='<Your Theme>'
  onError={(errors) => {
    console.log('onError: ', errors);
  }}
/>;
```

### Props

- `ca` บัญชีแสดงสัญญา (CA) ของบ้าน/คอนโด ที่ต้องการแสดงข้อมูล
- `theme` Theme ที่เลือกใช้งาน ([รายการ Theme ทั้งหมด](#sdk-theme))
- `onError` Callback error function

<br/><br/>

## ลงทะเบียนเครื่องวัดฯ

```js
import { registerCa } from './<Path to your MEAEnergy>/services';

registerCa(options);
```

### options

- `ca` เลขที่แสดงสัญญา (CA)
- `projectCode` รหัสโครงการ
- `projectName` ชื่อโครงการภาษาไทย
- `projectNameEn` ชื่อโครงการภาษาอังกฤษ
- `projectAddress` ที่อยู่โครงการภาษาไทย
- `projectAddressEn` ที่อยู่โครงการภาษาอังกฤษ

<br/><br/>

# SDK Theme

- `mea` MEA theme
- `light` Light theme
- `dark` Dark theme

![](./static/all-theme.jpg)
